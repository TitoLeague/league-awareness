# League Awareness

Discord: https://discord.gg/dqTeShmAXw


![Feature overview](overview-features-unedited.mp4)

![Phone overview](phone-example.png)

## Read Before Using

### 1. Add the directory to which you extract `League Awareness` to the exception list of Windows Defender
> `League Awareness` is packed and obfusacted with VMProtect. This causes anti-virus software to flag it as a false positive. If you do not trust this software, run it on a VM.

![Security exception](security-exception.mp4)

### 2. Extract the zip file that you acquired from the discord server
![Extraction](extraction.mp4)

### 3. Acquire your HWID
> Simply run the extracted `.exe` or the generated `run.bat` file and copy the HWID shown in the terminal. *Note that you should always run `League Awareness` using the `run.bat` file when you are actually using it, this is due to `League Awareness` requiring a PowerShell execution environment.*

### 4. QUESTION: Why does this `League Awareness` require administrator privileges?
> `League Awareness` requires this privilege since it will prevent the other non-adminstrator process from opening our process. Additionally, it will patch the file permissions of `League Awareness` in such a way that it disallows any non-administrator users to read, write or execute our executable. The goal of this is to prevent anti-cheat signature scanning from picking up our sent.

### 5. QUESTION: Why can't I execute the executable after the first run? 
> This is due to the file permission patch mentioned above. Please continue using our program with the freshly generated `run.bat`.

### 6. QUESTION: How and why do I sync my time on Windows 10?
> The syncing of time is required for the licensing system to verify the validity of your license file.

![Fix time sync](fix-time-sync.mp4)

### 7. Allow firewall access
> We need firewall access to host the webapp on your PC, which you can access through the browser as described below.

![Allow firewall access](allow-firewall-access.mp4)

### 8. Access `League Awareness`
> Navigate your browser of choice to the following url: http://127.0.0.1:42070

![Access via browser](access-via-browser.mp4)

### 9. QUESTION: What do the three popup input messages mean? And how can I reconfigure them?
*If you are not familiar with any of these terms described below, leave all values to default*
> The first input message specifies where the webapp should retreive the match data from. This is usually your local, LAN or WLAN IP with port `42069`. The protocol is `ws://` which stands for WebSocket. Example url: `ws://<host-ip>:42069`. 

> The second input message asks about the polling rate in milliseconds. This basically means how often should the webapp request a new state from the match API. The lower this number is, the higher the CPU usage of both your browser and `League Awareness` will be. Leave it on the default of `10` milliseconds and increase it if you are experience lag.

> The third and last input message asks if you would like to receive text-to-speech message about in-game events such as for example enemy champions using large cooldowns or wards placed close to you.

### 10. QUESTION: Can I share `League Awareness` with my teammates?
> Yes you can! If you are familiar with port forwarding. Forward the port 42070 and 42069 inside of your router to you LAN IP. Now you can share your external IP with your team and have every teammate access `League Awareness`. Use the following URL to make `League Awareness` aware of what player is using it: `http://<external-ip>:42070/?summoner_name=<summoner-name>` where `<external-ip>` is your IP address and `<summoner-name>` is the summoner name of the player using `League Awareness`. Encode the summoner name using simple http encoding: https://www.urlencoder.io/.

> Alternatively, you can use the following url to specify what champion you are playing: `http://<external-ip>:42070/?champ=<champion-name>` where `<external-ip>` is again your IP address and `<champion-name>` is the name of the champion the player using `League Awareness` is playing. Again encode the name using: https://www.urlencoder.io/.

### 11. QUESTION: How can I use the mobile version?
> Append `?compact=1` or `&compact=1` to the url, depending on if you already have a GET parameter defined



